import re
# https://regexone.com/references/python
regex = r"([a-zA-Z]+) (\d+)"
if re.search(regex, "June 24"):

    match = re.search(regex, "June 24")
    
    # This will print [0, 7), since it matches at start and end
    print "Match at index %s, %s" % (match.start(), match.end())
    
    # Groups contain matched values. 
    #    match.group(0) always returns the fully matched string
    #    match.group(1) match.group(2), ... will return the capture
    #            groups in order from left to right in the input string
    #    match.group() is equivalent to match.group(0)
    
    # So this will print "June 24"
    print "Full match: %s" % (match.group(0))
    # So this will print "June"
    print "Month: %s" % (match.group(1))
    # So this will print "24"
    print "Day: %s" % (match.group(2))
else: 
    # If no match, None is returned
    print "The regex pattern does not match. :("